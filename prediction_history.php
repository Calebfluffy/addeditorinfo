<!-- // Prediction algorithm SVM by Xinyu Li
//Prediction Indicator RSI by Xinyu Li
//Prediction Indicator EMA by Xinyu Li
// debugged by:Xinyu Li -->
<?php
session_start();
require 'DBconnect.php';
$symbol=$_SESSION['symbol'];
//get RSI
$stock_RSI_qry = "SELECT RSI FROM Stocks_his_pre WHERE Symbol='$symbol' ORDER BY Date desc LIMIT 1";
$stock_RSI = mysqli_query($connect,$stock_RSI_qry);
$stock_RSI_result=mysqli_fetch_array($stock_RSI);
$RSI=$stock_RSI_result['RSI'];
//get EMA
$stock_ema_qry = "SELECT Predict FROM ema_pre WHERE Symbol='$symbol'";
$stock_ema = mysqli_query($connect,$stock_ema_qry);
$stock_ema_result=mysqli_fetch_array($stock_ema);
$EMA=$stock_ema_result['Predict'];
//get SVM
$stock_svm_qry = "SELECT Predict FROM svm_pre WHERE Symbol='$symbol'";
$stock_svm = mysqli_query($connect,$stock_svm_qry);
$stock_svm_result=mysqli_fetch_array($stock_svm);
$SVM=$stock_svm_result['Predict'];
//get ANN
$stock_ann_qry = "SELECT Predict FROM ann_pre WHERE Symbol='$symbol'";
$stock_ann = mysqli_query($connect,$stock_ann_qry);
$stock_ann_result=mysqli_fetch_array($stock_ann);
$ANN=$stock_ann_result['Predict'];
//get max in 10 days
$stock_MAX_qry = "SELECT MAX(Close) FROM Stocks_history WHERE Symbol='$symbol' ORDER BY StockID Desc LIMIT 10";
$stock_MAX = mysqli_query($connect,$stock_MAX_qry);
$stock_MAX_result=mysqli_fetch_array($stock_MAX);
$MAX=$stock_MAX_result['MAX(Close)'];
//get average in last one year
$stock_AVG_qry = "SELECT AVG(Close) FROM Stocks_history WHERE Symbol='$symbol'";
$stock_AVG = mysqli_query($connect,$stock_AVG_qry);
$stock_AVG_result=mysqli_fetch_array($stock_AVG);
$AVG=$stock_AVG_result['AVG(Close)'];
//get min in last one year
$stock_MIN_qry = "SELECT MIN(Close) FROM Stocks_history WHERE Symbol='$symbol'";
$stock_MIN = mysqli_query($connect,$stock_MIN_qry);
$stock_MIN_result=mysqli_fetch_array($stock_MIN);
$MIN=$stock_MIN_result['MIN(Close)'];


echo '<h1>Long term prediction</h1>';
echo '<h4>RSI: '.$RSI.'</h4>';
echo '<h4>SVM: '.$SVM.'</h4>';
echo '<h4>ANN: '.$ANN.'</h4>';

echo '<h4>EMA: ';
//SVM prediction
if($EMA==-1){
    echo '<button type="button" class="btn btn-danger">Sell</button>';
}
else if($EMA==0){
    echo '<button type="button" class="btn btn-secondary">Hold</button>';
}
else{
    echo '<button type="button" class="btn btn-success">Buy</button>';
}
echo '</h4>';

echo '<br />';
echo '<h4>MAX past 10 days: '.$MAX.'</h4>';
echo '<h4>MIN past year: '.$MIN.'</h4>';
echo '<h4>Average past year: '.$AVG.'</h4>';

?>
