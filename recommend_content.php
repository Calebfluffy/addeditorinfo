<!-- // written by:Yuwei Jiang, Cheng Chen
// debugged by:Chenfan Xiao -->
<html>
<head>

</head>
<body>
  <?php
    include('DBconnect.php');
    // check system stock list
    $check_ema_query = "SELECT * FROM ema_pre WHERE Predict='1' ";
    $check_ema_sys = mysqli_query($connect,$check_ema_query);
    echo '<p><br /><h1>Recommend Stocks: </h1><br />';
    echo '<div class="table-responsive"><table class="table table-striped">';
    echo '    <thead>
      <tr>
        <th>Name</th>
        <th>Latest Price</th>
      </tr>
    </thead>
        <tbody>';
    while($ema_stock_row = mysqli_fetch_array($check_ema_sys)){
        $symbol=$ema_stock_row['Symbol'];
        $check_all_query = "SELECT * FROM sys_stock WHERE Symbol='$symbol' LIMIT 1";
        $check_all_sys = mysqli_query($connect,$check_all_query);
        $sys_stock_row = mysqli_fetch_array($check_all_sys);
        $realtime_qry="SELECT Time,Price,Date FROM Stocks_realtime WHERE Symbol='$symbol' ORDER BY StockID desc limit 1";
        $realtime_result = mysqli_query($connect,$realtime_qry);
        if($realtime_result==false){
            echo "Mysql realtime data query failed. ";
        }
        $realtime_row = mysqli_fetch_array($realtime_result);
        echo '<tr><td><h4><a href="stock.php?s=',$symbol,'&ch=c">',$sys_stock_row['Name'],'</a></h4></td>';
        // echo '<td>',$ema_stock_row['Predict'],'</td>';
        echo '<td>',$realtime_row['Price'],'</td></tr>';
    }
    echo '</tbody></table></div></p>';

    ?>
</body>
</html>
