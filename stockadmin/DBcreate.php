<!-- // written by:Yuwei Jiang
// assisted by:Cheng Chen
// debugged by:Yuwei Jiang -->
<?php

    define("DB_HOST","localhost");
    define("DB_USER","root");
    define("DB_PWD","root");
    $connect = new mysqli(DB_HOST,DB_USER,DB_PWD );
     if ($connect->connect_error)
 	 {
 	 	die('Could not connect: ' . $connect->connect_error );
  	 }
    //create database
  	$sql = "CREATE DATABASE DEMO1";
    $res = mysqli_query($connect,$sql);
	if ($res === TRUE) {
	    echo "Database created successfully";
	} else {
	    echo "Error creating database: " . $connect->error;
	}

	// sq2 to create table realtime stocks
	$sql2 = "CREATE TABLE DEMO1.Stocks_realtime (StockID INT(11) NOT NULL  AUTO_INCREMENT, PRIMARY KEY(StockID), Symbol VARCHAR(30) NOT NULL, Company VARCHAR(30) NOT NULL, Price DECIMAL(8,4) NOT NULL, Date VARCHAR(30) NOT NULL, Time VARCHAR(30) NOT NULL, Volume INT(10) NOT NULL)";
    $res2 = mysqli_query($connect,$sql2);
	if ($res2 === TRUE) {
	    echo "Table Stocks_realtime created successfully";
	} else {
	    echo "Error creating table: " . $connect->error;
	}

	// sq3 to create table history stocks
	$sql3 = "CREATE TABLE DEMO1.Stocks_history (StockID INT(11) NOT NULL  AUTO_INCREMENT, PRIMARY KEY(StockID), Symbol VARCHAR(30) ,Date date NOT NULL, Open DECIMAL(8,4) NOT NULL, High DECIMAL(8,4) NOT NULL, Low DECIMAL(8,4) NOT NULL, Close DECIMAL(8,4) NOT NULL, Volume INT(10) NOT NULL)";
    $res3 = mysqli_query($connect,$sql3);
	if ($res3 === TRUE) {
	    echo "Table Stocks_history created successfully";
	} else {
	    echo "Error creating table: " . $connect->error;
	}

	//sq4 to create table user
    $sql4 = "CREATE TABLE DEMO1.user (userid INT(10) NOT NULL  AUTO_INCREMENT, PRIMARY KEY(userid), username VARCHAR(20) NOT NULL, password VARCHAR(40) NOT NULL, email VARCHAR(20) NOT NULL, regdate INT(11) NOT NULL)";
    $res4 = mysqli_query($connect,$sql4);
	if ($res4 === TRUE) {
	    echo "Table user created successfully";
	} else {
	    echo "Error creating table: " . $connect->error;
	}

    //sq5 to create table user
    $sql5 = "CREATE TABLE DEMO1.user_stock (usid INT(10) NOT NULL  AUTO_INCREMENT, PRIMARY KEY(usid), uid INT(11) NOT NULL, sym VARCHAR(10) NOT NULL)";
    $res5 = mysqli_query($connect,$sql5);
	if ($res5 === TRUE) {
	    echo "Table user created successfully";
	} else {
	    echo "Error creating table: " . $connect->error;
	}

    //sq6 to create system stock list
    $sql6 = "CREATE TABLE DEMO1.sys_stock (stockid INT(10) NOT NULL  AUTO_INCREMENT, PRIMARY KEY(stockid), symbol VARCHAR(10) NOT NULL, Name VARCHAR(30) NOT NULL)";
    $res6 = mysqli_query($connect,$sql6);
	if ($res6 === TRUE) {
	    echo "Table user created successfully";
	} else {
	    echo "Error creating table: " . $connect->error;
	}

	//sq7 to create stocks history prediction
	$sql7="CREATE TABLE `DEMO1`.`Stocks_his_pre` ( `PreID` INT UNSIGNED NOT NULL AUTO_INCREMENT , `Symbol` VARCHAR(10) NOT NULL , `Date` DATE NOT NULL , `RSI` DECIMAL(8,4) NOT NULL , PRIMARY KEY (`PreID`))";
	$res7= mysqli_query($connect,$sql7);
	if ($res7 === TRUE) {
	    echo "Table user created successfully";
	} else {
	    echo "Error creating table: " . $connect->error;
	}

	//sq8 to create ema_pre
	$sql8="CREATE TABLE `DEMO1`.`ema_pre` ( `Symbol` VARCHAR(10) NOT NULL , `Predict` INT(2) NULL )";
	$res8= mysqli_query($connect,$sql8);
	if ($res8 === TRUE) {
	    echo "Table user created successfully";
	} else {
	    echo "Error creating table: " . $connect->error;
	}

	//sq9 to create svm_pre
	$sql9="CREATE TABLE `DEMO1`.`svm_pre` ( `Symbol` VARCHAR(10) NOT NULL , `Predict` DECIMAL(7,4) NULL )";
	$res9= mysqli_query($connect,$sql9);
	if ($res9 === TRUE) {
	    echo "Table user created successfully";
	} else {
	    echo "Error creating table: " . $connect->error;
	}

	//sq10 to create b_pre
	$sql10="CREATE TABLE `DEMO1`.`b_pre` ( `Symbol` VARCHAR(10) NOT NULL , `Predict` DECIMAL(7,4) NULL )";
	$res10= mysqli_query($connect,$sql10);
	if ($res10 === TRUE) {
	    echo "Table user created successfully";
	} else {
	    echo "Error creating table: " . $connect->error;
	}

	//sq11 to create ann_pre
	$sql11="CREATE TABLE `DEMO1`.`ann_pre` ( `Symbol` VARCHAR(10) NOT NULL , `Predict` DECIMAL(7,4) NULL )";
	$res11= mysqli_query($connect,$sql11);
	if ($res11 === TRUE) {
	    echo "Table user created successfully";
	} else {
	    echo "Error creating table: " . $connect->error;
	}


	mysqli_close;

?>
