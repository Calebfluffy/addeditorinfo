<!-- // written by:Yuwei Jiang
// assisted by:Cheng Chen
// debugged by:Jianning Xu -->
<?php

    session_start();
    if(!isset($_SESSION['userid'])){
        echo 'Please log in first. ';
        echo '<script language="javascript">history.go(-1);</script>';
        $userid = $_SESSION['userid'];
        $username = $_SESSION['username'];
        if($userid>10){
            echo 'Unauthorized user. ';
            echo '<script language="javascript">history.go(-1);</script>';
        }
    }
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Admin - System Stock Manager</title>
<!-- Bootstrap -->
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">
<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet" />
<link href="https://fonts.googleapis.com/css?family=Abel|Open+Sans:400,600" rel="stylesheet" />
<link href="adminmanage.css" rel="stylesheet" type="text/css" />
<!--Bootstrap ends-->
</head>

<body>
    <div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3 panel panel-default">
                <p class="text-center"><h1>Admin</h1></p>
                <h3>Alert:</h3>
                <?php
    require 'DBconnect.php';

    $symbol=$_GET['s'];
    $sname=$_GET['n'];
    $ope=$_GET['ope'];


    $check_sys_stock_duplicate = "SELECT stockid FROM sys_stock WHERE symbol='$symbol' LIMIT 1 ";
    $check_sys_duplicate = mysqli_query($connect,$check_sys_stock_duplicate);

    if($ope=="add"){
        //check duplicate
        //if exists
        if(mysqli_fetch_array($check_sys_duplicate)){
            echo 'Add failed. You have already added ',$symbol,' before. ';
        }
        //stock exists
        elseif(empty($_GET['n'])){
            echo 'Add failed. Stock Name cannot be empty. ';
        }
        else{
            $add_sys_qry = "INSERT INTO sys_stock(symbol,Name) VALUES('$symbol','$sname')";
            $add_sys_result = mysqli_query($connect,$add_sys_qry);
            if($add_sys_result){
                echo 'Add ',$symbol, ' successful!<br />';
            }
            else{
                echo 'Add failed!<br />';
            }
            //add ema_pre
            $add_ema_qry = "INSERT INTO ema_pre(Symbol) VALUES('$symbol')";
            $add_ema_result = mysqli_query($connect,$add_ema_qry);
            if($add_ema_result){
                echo 'Add ',$symbol, ' to ema_pre successful!<br />';
            }
            else{
                echo 'Add ema_pre failed!<br />';
            }
            //add svm_pre
            $add_svm_qry = "INSERT INTO svm_pre(Symbol) VALUES('$symbol')";
            $add_svm_result = mysqli_query($connect,$add_svm_qry);
            if($add_svm_result){
                echo 'Add ',$symbol, ' to svm_pre successful!<br />';
            }
            else{
                echo 'Add svm_pre failed!<br />';
            }
        }
    }
    elseif($ope=="remove"){
        //check duplicate
        if(mysqli_fetch_array($check_sys_duplicate)){
            //if exists
            $rem_sys_qry="DELETE FROM sys_stock WHERE symbol='$symbol'";
            $rem_sys_stock=mysqli_query($connect,$rem_sys_qry);
            if($rem_sys_stock){
                echo 'Remove stock ',$symbol,' successful.<br />';
            }
            else{
                echo 'Remove failed!<br />';
            }
            //remove from ema_pre
            $rem_ema_qry="DELETE FROM ema_pre WHERE symbol='$symbol'";
            $rem_ema_stock=mysqli_query($connect,$rem_ema_qry);
            if($rem_ema_stock){
                echo 'Remove stock ',$symbol,' from ema_pre successful.<br />';
            }
            else{
                echo 'Remove from ema_pre failed!<br />';
            }
            //remove from svm_pre
            $rem_svm_qry="DELETE FROM svm_pre WHERE symbol='$symbol'";
            $rem_svm_stock=mysqli_query($connect,$rem_svm_qry);
            if($rem_svm_stock){
                echo 'Remove stock ',$symbol,' from svm_pre successful.<br />';
            }
            else{
                echo 'Remove from svm_pre failed!<br />';
            }
        }
        else{
        //if not
            echo 'You do not have stock ',$symbol;
        }
    }
    else{
        echo 'Please choose a valid operation. ';
    }

    // check system stock list
    $check_all_query = "SELECT * FROM sys_stock WHERE 1";
    $check_all_sys = mysqli_query($connect,$check_all_query);
    echo '<p><br />System Stock List: <br />';
    while($sys_stock_row = mysqli_fetch_array($check_all_sys)){
        echo '      Symbol: ',$sys_stock_row['symbol'],'  Name: ',$sys_stock_row['Name'],' <a href="addsysstock.php?s=',$sys_stock_row['symbol'],'&ope=remove">delete</a><br />';
    }
    echo '</p>';
?>
        <form  action="addsysstock.php"   method="get" class="margin-base-vertical">
        <p class="input-group">
            <span class="input-group-addon">Stock Symbol:</span>
            <input    type="text"   name="s"  value=""  class="form-control input-lg"/><br/>
        </p>
        <p class="input-group">
            <span class="input-group-addon">Stock Name:</span>
            <input    type="text"   name="n"  value=""  class="form-control input-lg"/><br/>
        </p>
        <p class="text-center">
            <button name="ope" type="submit"  value="add" class="btn btn-success btn-lg" />Add</button>
            <button name="ope" type="submit"  value="remove" class="btn btn-success btn-lg" />Remove</button>
        </p>
        </form>
        <span><a href="index.php">Admin  </a>|<a href="../index.php">  Index</a></span>
    </div>
<!--panel ends-->
</div>
<!--row ends-->
</div>
<!--container ends-->
</body>
</html>
