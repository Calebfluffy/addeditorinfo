<!-- // written by:Cheng Chen
// assisted by:Jianing Xu
// debugged by:Chenfan Xiao -->
<?php

require 'DBconnect.php';

/*************List the ids of companies along with their name who have the average
 stock price lesser than the lowest of Google in the latest one year**********************/
$goog_MIN_qry = "SELECT MIN(Close) FROM Stocks_history WHERE Symbol='goog'";
$goog_MIN = mysqli_query($connect,$goog_MIN_qry);
$goog_MIN_result=mysqli_fetch_array($goog_MIN);
$goog_MIN=$goog_MIN_result['MIN(Close)'];
//echo 'gosdadfsdds ',$goog_MIN;

echo '<p><h3>Stocks with average price lower than Google: </h3></p>';
echo '<p><div class="table-responsive"><table class="table table-striped">';
    echo '    <thead>
      <tr>
        <th>Name</th>
        <th>Average Price</th>
      </tr>
    </thead>
        <tbody>';
$stock_name=mysqli_query($connect,"SELECT symbol FROM sys_stock");
//$stock_name_result=mysqli_fetch_array($stock_name);
while($output=mysqli_fetch_assoc($stock_name))
{
  $symbol=$output[symbol];
  $check_name_query = "SELECT * FROM sys_stock WHERE Symbol='$symbol'";
  $check_name_sys = mysqli_query($connect,$check_name_query);
  $name_stock_row = mysqli_fetch_array($check_name_sys);
  $name=$name_stock_row['Name'];
  $stock_AVG_qry = "SELECT AVG(Close) FROM Stocks_history WHERE Symbol='$symbol'";
  $stock_AVG = mysqli_query($connect,$stock_AVG_qry);
  if($stock_AVG==false)
  {
    echo "mysql fail";
  }
  $stock_AVG_result=mysqli_fetch_array($stock_AVG);
  $AVG=$stock_AVG_result['AVG(Close)'];
  //echo $AVG;
//echo '<h4>symbol:',$symbol,' average price of the year:',$AVG,'</h4><br />';

  if($AVG<$goog_MIN)
  {
    echo'<tr><td><h4><a href="stock.php?s=',$symbol,'&ch=c">',$name,'</h4></td>     <td>',$AVG,'</td></tr>';
  }

}//end while
echo '</tbody></table></div></p>';
echo '<p><h4><br />The minium price for Google over the last year is ',$goog_MIN,'</h4></p>';

?>
